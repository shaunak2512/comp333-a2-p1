/*
 * The structure of this class is similar to the Jury_Pool1 class. There are two crucial differences. 
 * You have to use a new graph reflecting 2-relationship. 
 * You have to compute 3 fields. The first field easy it is simply the original graph size.
 * The second field is the size of the pool of potential jurors and the third field is a list of juror 
 * id's satisfying the condition that there is no 2-relations between any two. These fields are exactly 
 * like the corresponding fields in Jury_Pool1. The names are slightly modified. 
 */
package jury;

import java.util.ArrayList;

public class Jury_Pool2 {
	int initialSize = 0;
	int juryPool2Size = 0;
	int[] juryPool2List;
	
	public Jury_Pool2(String s) {
		Jury_Relation2Graph candidateRelation = new Jury_Relation2Graph(s);
		
		/*
		 The string 's' represents the path to the input file. This part 
		 has been implemented.
		 */
		
		initialSize = candidateRelation.graphSize;
		juryPool2Size = calcPool2size(candidateRelation);
		juryPool2List = calcPool2List(candidateRelation);
	}
	
	int calcPool2size(Jury_Relation2Graph jG) {
		int poolSize = 0;
		
		/*Your code goes here. 
		The variable poolSize is given as a suggestion. You are free to change it if you wish.
		Of course you have to return some variable of integer type. It has been initialized to 0.  
		Note: 0 is not an acceptable value for the jury pool size. 
		You can write any other classes and methods 
		to help you calculate. 
		*/
		return poolSize;
	}
	static int[] calcPool2List(Jury_Relation2Graph jG) {
		
		ArrayList<Integer> jury = new ArrayList<Integer>();
		/*Your code goes here. 
		 * We have an integer array poolList. To create this array you will have to calculate 
		 * juryPoolSize first. You can change it. But make sure the function returns an array 
		 * of integers that represents an acceptable jury pool. 
		 * It is suggested that you create some classes and methods of your own and call them here. This 
		 * method and the one preceding can be considered 'wrapper' methods. 
		 */
		int[] degrees = jG.degreeArr;
		int i =0;
		while(!jG.candidate_List2.isEmpty()){
			Candidate curr = minDegrees(degrees,jG.candidate_List2);
			if(curr.id == -1)
				continue;
			if(curr.id>=0){
				jury.add(curr.id);
				jG.candidate_List2.remove(curr.id);
			}	
			i++;
			jG = removeNeighbours(curr,jG);
		}
		
		int[] poolList = new int[jury.size()];
		for(int j=0;i<jury.size();j++){
			poolList[j] = jury.get(i);
		}
		
		
		return poolList;
	}
	
	static Jury_Relation2Graph removeNeighbours(Candidate curr,Jury_Relation2Graph g){
		for(Candidate c :curr.adj_List){
			if(CandidateContains(c,g.candidate_List2))
				g.candidate_List2 = removeCandidate(c.id,g.candidate_List2);
		}
		return g;
	}
	
	static ArrayList<Candidate> removeCandidate(int n,ArrayList<Candidate> arr){
		for(int i=0;i<arr.size();i++){
			if(arr.get(i).id == n)
				arr.remove(i);
		}
		return arr;
	}
	
	static boolean CandidateContains(Candidate c, ArrayList<Candidate> arr){
		for(Candidate curr:arr){
			if(curr.id == c.id)
				return true;
		}
		return false;
	}
	
	static Candidate minDegrees(int[] deg,ArrayList<Candidate> cand){
		int min = -1;
		int index = -1;
		int[] idList = new int[cand.size()-1];
		for(int i =0;i<idList.length;i++){
			idList[i]=cand.get(i+1).id;
		}
		min = 0;
		for(int i=0;i<deg.length;i++){
			if(contains(i+1,idList)){
				if(deg[i]<min){
					min=deg[i];
					index = i+1;
				}		
			}
		}
		if(index>=0)
			return cand.get(index);
		return new Candidate(-1);
	}
	
	static boolean containsOnlyZero(ArrayList<Candidate> arr){
		if(arr.size()==1)
			if(arr.get(0).id == 0)
				return true;
		return false;
		
	}
	
	static boolean contains(int n, int[] arr){
		for(int i: arr){
			if(i==n)
				return true;
		}
		return false;
	}
	
}
