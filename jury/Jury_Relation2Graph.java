package jury;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/*This class relates to the second relationship in the assignment.
 * You have to create a new graph from original direct relation graph. 
 * The graph size will be the same as the original direct relation graph. The degree 
 * of a vertex is the number of edges incident on it. It is the same as the length of 
 * the adjacency list. So you have to compute the adjacency list of each vertex. The field candidate_List2
 * will contain the vertices (Candidates) and their respective adjacency list. 
 */
public class Jury_Relation2Graph {
	int graphSize = 0;
	ArrayList<Candidate> candidate_List2;
	int[] degreeArr;
	/*This variable gives the degree of each vertex in the graph. 
	 * The constructor takes the original graph as input and constructs the new
	 * graph. The two graphs have the same size. 
	 */
	public Jury_Relation2Graph(String s) {
		JuryGraph jG1 = new JuryGraph(s);
		graphSize = jG1.graph_Size;
		candidate_List2 = makeRelation2Graph(jG1);
		degreeArr = computeDegrees(candidate_List2);
	}
	static ArrayList<Candidate> makeRelation2Graph(JuryGraph jG){
		ArrayList<Candidate> canList2 = new ArrayList<>(jG.graph_Size+1);
		canList2.add(new Candidate(0));
		Map<Integer, Map<Integer,Integer>> degMap = bfs2Rel(jG);
		for(Map.Entry<Integer, Map<Integer,Integer>> m: degMap.entrySet()){
			Candidate curr = jG.candidate_List.get(m.getKey());
			ArrayList<Candidate> adj = curr.adj_List;
			for(Map.Entry<Integer, Integer> n: m.getValue().entrySet()){
				if(n.getValue() == 2){
					if(!temp(curr.adj_List,n.getKey())){
						curr.adj_List.add(getNode(n.getKey(),jG.candidate_List));
					}	
				}
			}
			canList2.add(curr);
		}
				
		/*
		 * Your code goes here. The input is graph representing direct relations among 
		 * candidates. You have to make new adjacency lists for each vertex reflecting 2-relations.
		 */
		
		return canList2;
	}
	
	static Candidate getNode(int n, ArrayList<Candidate> cl){
		for(Candidate c:cl){
			if(c.id == n)
				return c;
		}
		return new Candidate(-1);
	}
	
	static boolean temp(ArrayList<Candidate> adj, Integer i){
		for(Candidate c: adj){
			if(c.id == i)
				return true;
		}
		return false;
	}
	
	int[]  computeDegrees(ArrayList<Candidate> cList2) {
		int[] degAr = new int[graphSize];
		/*
		 * Your code goes here. It is quite straightforward to compute array 
		 * entries from the adjacency list of vertices. Use the "size()" function of 
		 * ArrayList class. 
		 */
		for(int i=0;i<graphSize;i++){
			degAr[i] = cList2.get(i+1).adj_List.size();
		}	
		return degAr;
	}
	
	
	static Map<Integer, Map<Integer,Integer>> bfs2Rel(JuryGraph jG){
		
		Map<Integer, Map<Integer,Integer>> degreeMap = new HashMap<Integer, Map<Integer,Integer>>();
		
		//do it for each candidate
		for(int i=1;i<jG.candidate_List.size();i++){
			jG = visitedNull(jG);
			//System.out.println();
			//System.out.print("For "+jG.candidate_List.get(i).id+": ");
			Map<Integer,Integer> lvlMap = new HashMap<Integer,Integer>();
			int level=0;
			Queue<Candidate> queue = new LinkedList<Candidate>();
			queue.add(jG.candidate_List.get(i));
			queue.add(null);
			ArrayList<Integer> temp = new ArrayList<Integer>();
			while(!queue.isEmpty()){
				//printQ(queue);
				//Dequeue
				Candidate curr = queue.poll();
				if(curr == null){
					level++;
					queue.add(null);
					if(queue.peek() == null) 
						break;
					else 
						continue;
				}
				if(curr.visited)
					continue;
				if(curr != null){
					curr.visited = true;
					//System.out.print(curr.id+":"+level+"\n");
					lvlMap.put(curr.id, level);
					temp.add(curr.id);
					for(Candidate c : curr.adj_List){	
						queue.add(c);
					}
				}
			}
			degreeMap.put(jG.candidate_List.get(i).id, lvlMap);
		}
		return degreeMap;
	}
	
	static JuryGraph visitedNull(JuryGraph jG){
		for(Candidate c: jG.candidate_List)
			c.visited = false;
		return jG;
	}
	
	static void printQ(Queue<Candidate> q){
		System.out.println();
		System.out.print("[ ");
		for(Candidate c: q){
			if(c == null)
				System.out.print("null, ");
			else
				System.out.print(c.id+", ");
		}
		System.out.print("]");
	}
	
	void printGraph() {
		for (int i = 1; i < this.graphSize+1;i++) {
			System.out.print(this.candidate_List2.get(i).id+": ");
			for (int j = 0; j < this.candidate_List2.get(i).adj_List.size(); j++) {
				System.out.print(this.candidate_List2.get(i).adj_List.get(j).id + ",");
			}
			System.out.println('\n');
		}
		
	}

}
