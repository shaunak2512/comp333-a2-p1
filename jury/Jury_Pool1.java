package jury;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Jury_Pool1 {
int initialSize = 0;
static int juryPoolSize = 0;
int[] juryPoolList;

/*In the constructor below a string is passed as parameter to first 
 * The variable "initialSize" represents the size of the initial list of candidates.
 * The variable "juryPoolSize" is the size of the final pool of potential jurors without any 'relationship' 
 * in the first sense. This number is unique for a given input list of candidates. 
 * The variable "juryPoolList" is an integer array containing a list of potential juror id's. These lists 
 * can vary. But of course they must all satisfy the basic criterion that there is no 'relationship' 
 * (in the first sense) between any pair. 
 * The class JuryGraph represents the basic graph of direct relations. 
 */

public Jury_Pool1(String s) {
	JuryGraph candidateRelation = new JuryGraph(s);
	
	/*
	 The string 's' represents the path to the input file. This part 
	 has been implemented.
	 */
	
	initialSize = candidateRelation.graph_Size;
	juryPoolSize = calcPoolsize(candidateRelation);
	juryPoolList = calcPoolList(candidateRelation);
}


int calcPoolsize(JuryGraph jG) {
	int poolSize = 0;
	/*Your code goes here. 
	The variable poolSize is given as a suggestion. You are free to change it if you wish.
	Of course you have to return some variable of integer type. It has been initialized to 0.  
	Note: 0 is not an acceptable value for the jury pool size. 
	You can write any other classes and methods 
	to help you calculate. 
	*/
	ArrayList<ArrayList<Integer>> jury = bfs(jG);
	return jury.size();
}
int[] calcPoolList(JuryGraph jG) {
	ArrayList<ArrayList<Integer>> juryPool = bfs(jG);
	int[] poolList = new int[juryPool.size()];
	/*Your code goes here. 
	 * We have an integer array poolList. To create this array you will have to calculate 
	 * juryPoolSize first. You can change it. But make sure the function returns an array 
	 * of integers that represents an acceptable jury pool. 
	 * It is suggested that you create some classes and methods of your own and call them here. This 
	 * method and the one preceding can be considered 'wrapper' methods. 
	 */
	for(int i=0;i<juryPool.size();i++){
		poolList[i] = juryPool.get(i).get(0);
	}
	
	return poolList;
	
}

static ArrayList<ArrayList<Integer>> bfs(JuryGraph jG){
	ArrayList<ArrayList<Integer>> juryPool = new ArrayList<ArrayList<Integer>>();
	Queue<Candidate> queue = new LinkedList<Candidate>();
	//do it for each candidate
	for(int i=1;i<jG.candidate_List.size();i++){
		queue.add(jG.candidate_List.get(i));
		ArrayList<Integer> temp = new ArrayList<Integer>();
		while(!queue.isEmpty()){
			//Dequeue
			Candidate curr = queue.poll();
			if(curr.visited)
				continue;
			curr.visited = true;
			temp.add(curr.id);
			for(Candidate c : curr.adj_List){
				queue.add(c);
			}
		}
		if(!temp.isEmpty())
			juryPool.add(temp);
	}
	return juryPool;
}


}



